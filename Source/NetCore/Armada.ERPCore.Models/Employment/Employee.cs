﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Armada.ERPCore.Models.Operationals;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Armada.ERPCore.Models.Employment
{
    public class Employee : Person
    {
        public string Title { get; set; }

        [BsonDateTimeOptions]
        public DateTime JoiningDate { get; set; }

        public User UserData { get; set; }

        public Department Department { get; set; }

        public int AttendanceDatabaseId { get; set; }

        public DateTime LastUpdatedOn { get; set; } = DateTime.Now;

    }
}
