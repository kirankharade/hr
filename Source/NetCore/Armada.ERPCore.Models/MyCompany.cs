﻿using System.Collections.Generic;
using Armada.ERPCore.Models.Business;
using Armada.ERPCore.Models.Employment;
using Armada.ERPCore.Models.Operationals;

namespace Armada.ERPCore.Models
{

    public class MyCompany : Organization
    {
        public List<Employee> Employees { get; set; }

        public Portfolio BusinessPortfolio { get; set; }

        public Finance Financials { get; set; }

        public List<Department> Departments { get; set; }

    }
}

