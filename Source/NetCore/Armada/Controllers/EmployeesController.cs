﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Armada.DataAccess.Interfaces;
using Armada.ERPCore.Models.Employment;
using Armada.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Armada.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeRepository _employeesRepository = null;

        public EmployeesController(IEmployeeRepository repository)
        {
            _employeesRepository = repository;
        }

        // GET api/values
        [NoCache]
        [HttpGet]
        public Task<IEnumerable<Employee>> Get()
        {
            return GetEmployees_Internal();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Task<Employee> Get(int id)
        {
            return GetEmployeeById_Internal(id);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Employee value)
        {
            _employeesRepository.AddEmployee(value);
            return CreatedAtAction("Get", new { id = value.ID }, value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Employee value)
        {
            value.ID = id;
            _employeesRepository.UpdateEmployee(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _employeesRepository.RemoveEmployee(id);
        }

        #region Internals

        private async Task<IEnumerable<Employee>> GetEmployees_Internal()
        {
            return await _employeesRepository.GetAllEmployees();
        }

        private async Task<Employee> GetEmployeeById_Internal(int id)
        {
            return await _employeesRepository.GetEmployee(id) ?? new Employee();
        }

        #endregion

    }
}
