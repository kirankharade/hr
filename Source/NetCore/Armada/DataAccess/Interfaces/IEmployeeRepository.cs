﻿using Armada.ERPCore.Models.Employment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Armada.DataAccess.Interfaces
{
    public interface IEmployeeRepository
    {
        Task<IEnumerable<Employee>> GetAllEmployees();

        Task<Employee> GetEmployee(int id);

        Task AddEmployee(Employee item);

        Task<bool> RemoveEmployee(int id);

        Task<bool> RemoveAllEmployees();

        Task<bool> UpdateEmployee(int id, Employee item);
    }
}
