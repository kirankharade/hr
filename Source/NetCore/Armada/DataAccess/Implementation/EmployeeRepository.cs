﻿using Armada.DataAccess.Interfaces;
using Armada.ERPCore.Models.Employment;
using Armada.Settings;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Armada.DataAccess.Implementation
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly HRContext _context = null;

        public EmployeeRepository(IOptions<DBConnectionSettings> settings)
        {
            _context = HRContext.GetInstance(settings);
        }

        public async Task<IEnumerable<Employee>> GetAllEmployees()
        {
            try
            {
                return await _context.Employees.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task AddEmployee(Employee item)
        {
            try
            {
                var filter = Builders<Employee>.Filter.Eq(x => x.ID, item.ID);
                    //& Builders<Employee>.Filter.Eq(x => x.FirstName, item.FirstName)
                    //& Builders<Employee>.Filter.Eq(x => x.LastName, item.LastName);

                var o = _context.Employees
                                .Find(filter)
                                .FirstOrDefaultAsync()
                                .Result;

                if (o == null)
                {
                    await _context.Employees.InsertOneAsync(item);
                }
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<Employee> GetEmployee(int id)
        {
            try
            {
                var filter = Builders<Employee>.Filter.Eq("ID", id);

                return await _context.Employees
                                .Find(filter)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<bool> RemoveAllEmployees()
        {
            try
            {
                DeleteResult actionResult = await _context.Employees.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<bool> RemoveEmployee(int id)
        {
            try
            {
                DeleteResult actionResult = await _context.Employees.DeleteOneAsync(
                     Builders<Employee>.Filter.Eq("ID", id));

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<bool> UpdateEmployee_Title(string id, string title)
        {
            var eID = int.Parse(id);
            var filter = Builders<Employee>.Filter.Eq(s => s.ID, eID);
            var update = Builders<Employee>.Update
                            .Set(s => s.Title, title)
                            .CurrentDate(s => s.LastUpdatedOn);
            try
            {
                UpdateResult actionResult = await _context.Employees.UpdateOneAsync(filter, update);

                return actionResult.IsAcknowledged
                    && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<bool> UpdateEmployeeDocument(int id, string title)
        {
            var item = await GetEmployee(id) ?? new Employee();
            item.Title = title;
            item.LastUpdatedOn = DateTime.Now;

            return await UpdateEmployee(id, item);
        }

        public async Task<bool> UpdateEmployee(int id, Employee item)
        {
            try
            {
                ReplaceOneResult actionResult = await _context.Employees
                                                .ReplaceOneAsync(n => n.ID.Equals(id)
                                                                , item
                                                                , new UpdateOptions { IsUpsert = true });
                return actionResult.IsAcknowledged
                    && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
    }
}
