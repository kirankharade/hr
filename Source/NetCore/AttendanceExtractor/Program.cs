﻿using Armada.ERPCore.Models.Employment;
using Armada.ERPCore.Models.HR.Attendance;
using Armada.ERPCore.Models.Operationals;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AttendanceDataPusher
{
    class Program
    {
        static HttpClient _client = new HttpClient();
        static List<Employee> _employees = new List<Employee>();
        static List<AccessEvent> _accessEvents = new List<AccessEvent>();
        static List<AccessPoint> _accessPoints = new List<AccessPoint>();

        static void Main(string[] args)
        {
            LoadJsonData();

            _client.BaseAddress = new Uri("https://localhost:5001/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                foreach (var o in _employees)
                {
                    var res = Add(o);
                }
                foreach (var o in _accessPoints)
                {
                    var res = Add(o);
                }
                foreach (var o in _accessEvents)
                {
                    var res = Add(o);
                }
            }
            catch(Exception exp)
            {
                Console.Write(exp.Message);
            }
        }

        private static void LoadJsonData()
        {
            var employeesStr = System.IO.File.ReadAllText(@"C:\Temp\Attendance\Employees.txt");
            _employees = JsonConvert.DeserializeObject<List<Employee>>(employeesStr);

            var accessEventsStr = System.IO.File.ReadAllText(@"C:\Temp\Attendance\AccessEvents.txt");
            _accessEvents = JsonConvert.DeserializeObject<List<AccessEvent>>(accessEventsStr);

            var accessPointsStr = System.IO.File.ReadAllText(@"C:\Temp\Attendance\AccessPoints.txt");
            _accessPoints = JsonConvert.DeserializeObject<List<AccessPoint>>(accessPointsStr);
        }

        static async Task<Uri> Add(Employee employee)
        {
            var json = JsonConvert.SerializeObject(employee, Formatting.Indented);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PostAsync("api/Employees", httpContent);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location; // return URI of the created resource.
        }

        static async Task<Uri> Add(AccessEvent obj)
        {
            var json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PostAsync("api/AccessEvents", httpContent);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location; // return URI of the created resource.
        }

        static async Task<Uri> Add(AccessPoint obj)
        {
            var json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PostAsync("api/AccessPoints", httpContent);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location; // return URI of the created resource.
        }

        static async Task<Uri> Add(Department obj)
        {
            var json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PostAsync("api/Departments", httpContent);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location; // return URI of the created resource.
        }
    }
}
